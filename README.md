## sound 

Convert image into sounds.


- add sound samples (.wav) to the `voice` folder
- run 
`./trim.sh` to trim start and end silences  
- run 
`python3 process.py --file images/kandisky01.jpg --cols 100` 
to create audio tracks


```
{--_-________+__________+___________-----?------???--??-]]???]]}\?]]][[[[[[}}}}{{{{1{11)))(((((||||/
)__+_+_++++++~+~~~++~+~~~~~++++__+____-__-__-___[({?---------?]JB}]]?]?]]]]]][[[}}[}}{{{{11)1()(((||
[_++~~~~~~~~~~~~~~~~~~~~~~~~~~~~+~++___++____1Yc(__--_-_11-_--?kc------????]?]]]][[[[[[}}{}{{{{1)()(
-~~+~~<<<<<<<<<<<<~~~~~~~<<<~~<<~~~+++______co1____--tQkZu_-_(Q\---_------???]??]]]][][[[[[}{{1{111|
+~~~<<<><<<>><<<<<<>+--~<<><<~<<<~~~+_+_~_-rBt-__++jdC\_}{(tvx[-_---------????????]]][[[[[[}}{}{{{1(
~~+<>>>>>>><<<<<<<<<?11_><><~~~~~~+++______Q%|-___Cx-___----jn\()[-_-------?-?-????]]]]]][[[[[}}}}1)
~~<>>>>>>>>>>>>>>>>><>>>]-[)[~~+~~~+_[XZ__[tx|))|Xn1)))())11czt(||(_-----------?-????]]]][[[[[[}}11)
~<<>>ii>>i>>i>>iiii>i>ii_n-+[))+<<~_[W%djrt\\\||\u)((((((((\xUC0QYX|}?----{){--???]??]]][[][}[[}}11(
~~<>>i>>iiiii~>iiii>i>i-~i{_><>><<<~+%%bt\||\(LXXzXXvvjfj(fjx\jYJLc((\(|(-!!<[{[--???]?]][[[[[[[}{{1
<<<>>>>>>>ii<]iiiiiiiiiii>i?i>>>><<~~8%b//||\(wv})czvnft|)rzY(|[uc|)))|)1/{_!!!(_-???????]]][[[[[}{1
<>>iiiiiiiii/!ii>iiiiii!!!ii>>i>>t<~+8&hxt(||||X-]pw0J/\{_>!n111)|xvv/()||\||{+|-??-?????]][[[[[[}{1
<>>i>iiii!!if<ii->iii!!-i!iiii>>_/1|1qda#*hOx()z_?qqmQ\|[+>ix11111)1\vYYx\\t/\\\(?-???---?]][[[[[}}1
~>iii!!iiiii{nii!\>i!!lt!!iii>i]~+_~>(|(_1ukM0)z_?qqOL||[~i!j))1tzLmqmLUJQUj))11(Xt}??-?-????]][[[}}
<>ii!ii!!!!!!uu~ii}{+!Ir>i!iii]iI_lii)1];!l>JWJz_?dp0L||}~i!f)|CwpppdbddqLjUOCn((YLf]?]????]]?]][[}{
<>ii!ii!!!!!!itQt~!!_[]r}}+ii!{!;?l!i1{?:!!lIkwn(/wmQZff\1}?cxXhkbJJmYUUZLj/|rYOt|||j[]???]]]]]][[}{
<>ii!!i!!!!!!!!_nLzt{?>uiiiiii}!;-lli1}?:lll>kt)1/1)11|\f11/Zao8Wn]tvniic|njjjrrxxjrjt]??]?]??]]][}{
>ii!!!!l!!!lll!!!>{/rcv|JYxii>>]l?lli1}-;Il-z\11[Z1{c/u|n11fwaabXf?\))X]fx/f/t/tfrjjfj)?]???]??]][}}
>ii!!l!llllllll!l!!!!!>x!<ii!iii]x[[?jj|?)ff|||||U[]v)v/n{{t0OZXzunnxunkUUzj\uzUJCCJUYf]]{{]]]]]]][}
>i!ll!llll!ll!!!l!!!!!?t!iiiii>_LZ)_>k8CnQLCJJUUU|[?Jcz*O-?/U?jjj\|(||||qZ]]]\j{__---?]/O%%mr[[][][}
i!!lll!lllllll!!!!!<1r[c?x}[1[+cLZOZ(q8d#8q+~~++~wdpQtx&d--/Y()11{++____w1O?_-[fj1??}cb@@@@@@oX1[[[}
i!!ll!l!!llll!!i[)|QJU[n>r?~]]][}m)()q%ZC8q~~~~+<bqwu_(&d-_fz+_++_l~+___q~(k|---{f(|**Ou)|fvYQqbLx}[
i!!l!!l!!ll!<(xUvcJLUv[v<n]~[1>>?qi<<d%Ztow~---?_LjfC|cZ0nnf1__]/?II~+_+d~-{C\-)C*n)nj(]]]]]][[[[}1}
il!lll!!!?|nXvn){)\jUr}z(u/|\1ii{di<~mBmfcq~++_+~n_~X?1UU/\vx?f)~~;II<++h~__1YLJf]??(nnr\[]]?]]]]][}
i!i!!<)fcvuuunf(1{){Ut[{iiiiiiii{b!<~QBwf/Y<<<<~~x-+n~[LC|\vvj_++~l!!!+~&~](\--------?|unvf1]]?]][[}
ii_\xYvuxnnxrxf<___lj|[[<~~~>iii]Wl<~C%C){)~~~~~_x-[J|tzUxYLf+++~+++++_+z+-__[__------?[rx|}]??][[}}
i]|111))\\txfj|/tfxnu){{_--+<iii]8l<~J%X1XcjcJf~)u1fx_1Zo/_Yf+_++++_-jJmqwQcv)_------??-???]]]]][[}{
iiiiiiii!i!i!!!!iiiiii/_<<<>iiii1%!<~z%bCzX(Coz+f1_]})LbQ__n)+++_+_+hw(]?fYQoML(----?--?-??]]]]][[}{
>iii!!i!!!!!!!!!!!!i!!j>!!!!!!i>)%~<~u8Qr\UmO//_n}+{Cn/uJ-_+~+~~+++_Ja[|\]---xWBm\(|]-?-????]][][}{1
>iii!iii!ii!!!!!!!i!!!)iiiiiii>>18}<~v%dph*bJ[(+)(Yc[_/cU?++~~++_+___Uqwv|{11]-j1-----????]]]][[[}{{
>iiiiiii!i_!iiii!<i>+ii!!!!i!i>>]J~><n%Qft//\|]{XY[~+_jzU}++++++__]|t[-?f#0--------?--??]]]]][[}}}}{
<>>>ii>>>i]!i+ii>>__<__~iiiiiii>>><><j%z_+<~~]cC{~+~++fUY?_+~+__](//ttfcO8a----_--?-???]][]}[[[}{}[1
>>>>ii>>__1__]___+++_]][]]_~~~~++<>><(%c_~~}zL|~~~~~++-]?+++]f\fj/|\rczvx|--_----????]?}(\zv}}}{}{}1
<~>>>>>>i>-i>~>>>>{1}[]]}{1][-~~>>><<)%c_{zQ|~~~~~~~+++_[fJcr-_____+_--_---_---?]]]?]]1rnLctx{{{1{))
~<~~><<<>>_>>>>>>>>i>>>>~~>[{]{1~<<<~)8JQqx-+~~+++__1rUQJf-______+__--_-----??][][]][}[1{xfY{{1{111)
+~~~~~~<~<<<~<<<~<<<<<>>>>><<<<~+~~~+(Qu|-++++++_-?[]-_-______------????]??]]][[[[[}}[}}{|)1{{1{{11)
```



