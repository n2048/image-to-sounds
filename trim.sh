mkdir -p trim
rm trim/*.*
for i in voice/*; do 
	inName=`echo "$i" | cut -d'.' -f1`
	outName=`echo "$inName" | cut -d'/' -f2`
  	echo $inName
  	ffmpeg -i $i -ac 1 -af silenceremove=1:0:-50dB trim/$outName.mp3
done
