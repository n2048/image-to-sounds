# Python code to convert an image to ASCII image. 
import sys, random, argparse 
import numpy as np 
import math 
from pydub import AudioSegment

from PIL import Image
import glob

txtfiles = []

# silence = AudioSegment.from_wav("silence.wav")
silence = AudioSegment.from_mp3("silence.mp3")


# 70 levels of gray 
gscale = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1{}[]?-_+~<>i!lI;:,\"^`'. "
# gscale = '@%#*+=-:. '


def getAverageL(image): 
	# get image as numpy array 
	im = np.array(image) 

	# get shape 
	w,h = im.shape 

	# get average 
	return np.average(im.reshape(w*h)) 


def covertImageToSoundTracks(fileName, cols, scale): 
	""" 
	Given Image and dims (rows, cols) returns an m*n list of audio tracks
	"""
	# declare globals 
	global gscale 

	sounds = []
	
	for file in glob.glob("trim/*.mp3"): 
		sounds.append(AudioSegment.from_mp3(file))
		# sounds.append(AudioSegment.from_wav(file))

	# open image and convert to grayscale 
	image = Image.open(fileName).convert('L') 

	# store dimensions 
	W, H = image.size[0], image.size[1] 
	# print("input image dims: %d x %d" % (W, H)) 

	# compute width of tile 
	w = W/cols 

	# compute tile height based on aspect ratio and scale 
	h = w/scale 

	# compute number of rows 
	rows = int(H/h) 
	
	# print("cols: %d, rows: %d" % (cols, rows)) 
	# print("tile dims: %d x %d" % (w, h)) 

	# check if image size is too small 
	if cols > W or rows > H: 
		print("Image too small for specified cols!") 
		exit(0)

	soundTracks = []

	# generate list of dimensions 
	for j in range(rows): 
		y1 = int(j*h) 
		y2 = int((j+1)*h) 

		# correct last tile 
		if j == rows-1: 
			y2 = H 

		track = silence
		
		for i in range(cols): 

			# crop image to tile 
			x1 = int(i*w) 
			x2 = int((i+1)*w) 

			# correct last tile 
			if i == 2*cols-1: 
				x2 = W 

			# crop image to extract tile 
			img = image.crop((x1, y1, x2, y2)) 

			# get average luminance 
			avg = int(getAverageL(img)) 
			# print(avg)

			# look up sound 
			track += sounds[int((avg*len(sounds))/255)]			

		soundTracks.append(track);
		
	# return all tracks 
	return soundTracks 

def covertImageToAscii(fileName, cols, scale): 
	""" 
	Given Image and dims (rows, cols) returns an m*n list of Images 
	"""
	# declare globals 
	global gscale 

	# open image and convert to grayscale 
	image = Image.open(fileName).convert('L') 

	# store dimensions 
	W, H = image.size[0], image.size[1] 
	# print("input image dims: %d x %d" % (W, H)) 

	# compute width of tile 
	w = W/cols 

	# compute tile height based on aspect ratio and scale 
	h = w/scale 

	# compute number of rows 
	rows = int(H/h) 
	
	# print("cols: %d, rows: %d" % (cols, rows)) 
	# print("tile dims: %d x %d" % (w, h)) 

	# check if image size is too small 
	if cols > W or rows > H: 
		print("Image too small for specified cols!") 
		exit(0)

	# ascii image is a list of character strings 
	aimg = [] 

	# generate list of dimensions 
	for j in range(rows): 
		y1 = int(j*h) 
		y2 = int((j+1)*h) 

		# correct last tile 
		if j == rows-1: 
			y2 = H 

		# append an empty string 
		aimg.append("") 
				
		for i in range(cols): 

			# crop image to tile 
			x1 = int(i*w) 
			x2 = int((i+1)*w) 

			# correct last tile 
			if i == 2*cols-1: 
				x2 = W 

			# crop image to extract tile 
			img = image.crop((x1, y1, x2, y2)) 

			# get average luminance 
			avg = int(getAverageL(img)) 
			# print(avg)

			# look up ascii char 
			gsval = gscale[int((avg*len(gscale))/255)] 
			
			# append ascii char to string 
			aimg[j] += gsval

	# return txt image 
	return aimg 

# main() function 
def main(): 
	# create parser 
	descStr = "This program converts an image into ASCII art."
	parser = argparse.ArgumentParser(description=descStr) 
	# add expected arguments 
	parser.add_argument('--file', dest='imgFile', required=True) 
	parser.add_argument('--scale', dest='scale', required=False) 
	parser.add_argument('--out', dest='outFile', required=False) 
	parser.add_argument('--cols', dest='cols', required=False) 

	# parse args 
	args = parser.parse_args() 
	
	imgFile = args.imgFile 

	# set output file 
	outFile = 'out.txt'
	if args.outFile: 
		outFile = args.outFile 

	# set scale default as 0.43 which suits 
	# a Courier font 
	scale = 0.43
	if args.scale: 
		scale = float(args.scale) 

	# set cols 
	cols = 80
	if args.cols: 
		cols = int(args.cols) 

	# print('generating ASCII art...') 
	# convert image to ascii txt 
	aimg = covertImageToAscii(imgFile, cols, scale) 

	soundTracks = covertImageToSoundTracks(imgFile, cols, scale)

	trackNumber = 0
	for track in soundTracks:
		trackNumber += 1 
		track.export("output/" + str(trackNumber) + ".mp3", format="mp3")

	# open file 
	f = open(outFile, 'w') 

	# write to file 
	for row in aimg: 
		print(row)
		f.write(row + '\n') 

	# cleanup 
	f.close() 
	# print("ASCII art written to %s" % outFile) 

# call main 
if __name__ == '__main__': 
	main() 

